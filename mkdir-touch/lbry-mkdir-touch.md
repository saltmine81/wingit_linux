# The mkdir and touch commands

- `mkdir` is a terminal command used to create folders

- `touch` is used to create files

![The mkdir and touch commands](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/mkdir-touch/thumbnail_mkdir-touch-commands.png)


## Examples

Open up a terminal and try the examples below.


### Create a folder 

The command to create a folder in the current directory is:

`mkdir NewFolder`, where NewFolder is the name of the folder you want to create.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/mkdir-touch/1.png)


### Create a file

To create a new file, type the following command:

`touch NewFile.txt`

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/mkdir-touch/2.png)


### Tips

- Create multiple folder/files by adding more items in the `mkdir / touch` commands.

For example:

`mkdir Folder1 Folder2 Folder3`

`touch file1.txt file2.txt`

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/mkdir-touch/3.png)

- Specify full path to create a folder or file. Add option `mkdir -p ...` to create non-exsting folders if needed. In the example below option -v is also added to show what's been done.

For example:

`mkdir -pv /home/user/Documents/pdfs/AnotherNewFolder`

Assuming that folder **pdfs** do not exist, it will be created along with its subfolder **AnotherNewFolder**.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/mkdir-touch/4.png)


## Digging deeper

Type `mkdir --help` and `touch --help` to explore more.

Thanks for checking out this post.

/ **wing-it-linux**
