# @wing-it-linux

Storage of the documents used for my lbry channel.

The aim of the channel (right now) is to make short tutorials on terminal commands. There are more ideas in the making.

[https://lbry.tv/@wing-it-linux:e](https://lbry.tv/@wing-it-linux:e)

![](assets/banner.png)
