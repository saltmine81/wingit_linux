# The cp command

A terminal command used to copy files and folders on the system. 

![The cp command](thumbnail_cp-command.png)


## Examples

Open up a terminal and try the examples below.


### Copy a file

The general command to copy a file is:

`cp file1 file2`, where file1 is the file you want to copy to a new (or existing) file2

![](1.png)


### Copy a directory with content

To copy a folder and its content, add option `cp -r ...`. For example `cp -r Folder CopyOfFolder`

![](2.png)


### Tips

- Add option `cp -i ...` (for example `cp -i Sourcefile Destinationfile`) to get an overwrite prompt if the destinationfile already exists. This can be helpful to always add, so that files aren't overwritten by mistake. Option `cp -u ...` is a variant that only copies if the source file is newer than the destination file (or if the destination file does not exist).

![](3.png)

- Add option `cp -v ...` (for example `cp -v Sourcefile Destinationfile`) to get more information of what has been done when doing a cp command. 

![](4.png)

- Remember that several options can be used at once, and that full paths can be used in the command.  
Example:

`cp -iv /home/user/Downloads/my.pdf /home/user/Documents/pdfs/`

*In the picture below I'm working in my temp folder, so the paths are different but the principle the same.*

![](5.png)


## Digging deeper

Type `cp --help` or `man cp` to explore more.

Thanks for checking out this post.

/ **wing-it-linux**
