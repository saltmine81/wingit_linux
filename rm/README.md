# The rm command

A terminal command used to remove files and folders on the system. Be careful when using this command. 

![The rm command](thumbnail_rm-command.png)


## Examples

Open up a terminal and try the examples below.


### Remove a file

***Caution: Be 100% sure before using this command. There will be no questions asked.***

The general command to remove a file is:

`rm file.txt`

![](1.png)


### Remove a directory with content

***Caution: Be 100% sure before using this command. There will be no questions asked.***

To remove a folder and its content, add option `cp -r ...`:

`cp -r FolderWithContent`

![](2.png)


### Tips

- Add option `rm -i ...` (for example `rm -i file.txt`) to get a promt (y/n) before the command is executed.

![](3.png)


## Digging deeper

Type `rm --help` or `man rm` to explore more.

Thanks for checking out this post.

/ **wing-it-linux**
