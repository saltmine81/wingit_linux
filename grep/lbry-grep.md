# The grep command

Grep is a command used to search for patterns in a file or the output of another command.

![The grep command](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/thumbnail_grep-command.png)


## Examples

Open up a terminal and try the examples below.


### Searching a text file

First, lets create a text file with some content. This can be done with a command like this:

`lscpu > cpu-info.txt`

Explanation to the command above: The output of command **lscpu** is sent **>** to a file called **cpu-info.txt**. The file will be created if it doesn't exist.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/1.png)

Print the content of the file to the terminal with command:

`cat cpu-info.txt`

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/2.png)

Now we can use the grep command to search for patterns of interest. For example, lets search the term Model.

`grep Model cpu-info.txt`

2 lines were returned containing the term Model.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/3.png)

### Grep the output of a command

Instead of creating a new text file, as the example above, we can pipe `|` the output of a command through grep. Try it with this command:

`lscpu | grep Model`

Explanation to the command above: Send the output of command **lscpu** through command **grep** searching for **Model**

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/4.png)


### Tips

- If searching for term including a space, use qoutation marks: "search term".

- Add option `grep -i ...` (for example `grep -i "search term" file.txt`) to ignore case of the search term.

- Add option `grep --color=auto` to show color of the search term.

All this combined in one command:

`lscpu | grep -i --color=auto "model name"`

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/grep/5.png)


## Digging deeper

Type `grep --help` or `man grep` to explore more. There is a lot more to learn about the grep command.

Thanks for checking out this post.

/ **wing-it-linux**
