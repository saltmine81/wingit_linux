# The ls command

A terminal command used to list content on a system.

![The ls command](thumbnail_ls.png)

## Examples

Open up a terminal and try the examples below.

### ls (list)

Just using `ls` (or more likely `ls -color=auto` due to an alias set in .bashrc) prints the content of the current directory.

Folders are shown in bold type with color.

![ls](ls.png)

### ls -a (list all)

Using `ls -a` also prints hidden files and directories. These have a dot first in file/folder name.

![ls -a](ls-a.png)

### ls -l (list long)

Using `ls -l` prints in a long listing format. Now we also see file owner, date and size for example.

![ls -l](ls-l.png)

### ls -alh (list all-long-human)

Combining options like `ls -alh` prints **a**ll files/folders with **l**ong listing format and **h**uman-readable sizes.  

![ls -alh](ls-alh.png)

## Digging deeper

Type `ls --help` or visit [the official homepage](https://www.gnu.org/software/coreutils/ls) to explore.

Thanks for checking out my first post.

/ **wing-it-linux**
