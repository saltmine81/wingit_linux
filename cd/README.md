# The cd command

A terminal command used to navigate between the different folders on the system. 

![The ls command](thumbnail_cd-command.png)


## Examples

Open up a terminal and try the examples below.


### The basics

- Moving *forward/down*: `cd Folder`

- Moving *back/up*: `cd ..`

- Go to ~ (home): `cd`

![](1.png)
![](2.png)
![](3.png)


### Step by step navigating

A typical way of navigating is to use the `ls` and `cd` commands in turn. Check the folder content with `ls` and go to the next folder of choice with `cd NextFolder`. Repeat if needed.

![](4.png)


### Go directly to folder of choice

Typing for example `cd Folder/NextFolder/DestinationFolder` brings you directly to DestinationFolder. If you know where to go, this is more efficient than individual `cd` commands (`cd Folder`, `cd NextFolder` and `cd DestinationFolder`).

![](5.png)


### Tips

- Find out where you are with command `pwd`. This prints the current location.

![](6.png)

- Press `Tab` to autocomplete and save some keystrokes.


## Digging deeper

Type `man cd` to explore.

Thanks for checking out this post.

/ **wing-it-linux**
