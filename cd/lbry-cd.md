# The cd command 
A terminal command used to navigate between the different folders on the system. 

![The cd command](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/thumbnail_cd-command.png)


## Examples

Open up a terminal and try the examples below.


### The basics

- Moving *forward/down*: `cd Folder`

- Moving *back/up*: `cd ..`

- Go to ~ (home): `cd`

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/1.png)
![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/2.png)
![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/3.png)


### Step by step navigating

A typical way of navigating is to use the `ls` and `cd` commands in turn. Check the folder content with `ls` and go to the next folder of choice with `cd NextFolder`. Repeat if needed.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/4.png)


### Go directly to folder of choice

Typing for example `cd Folder/NextFolder/DestinationFolder` brings you directly to DestinationFolder. If you know where to go, this is more efficient than individual `cd` commands (`cd Folder`, `cd NextFolder` and `cd DestinationFolder`).

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/5.png)


### Tips

- Find out where you are with command `pwd`. This prints the current location.

![](https://gitlab.com/magnus_ivarsson/wingit_linux/-/raw/master/cd/6.png)

- Press `Tab` to autocomplete and save some keystrokes.


## Digging deeper

Type `man cd` to explore.

Thanks for checking out this post.

/ **wing-it-linux**
